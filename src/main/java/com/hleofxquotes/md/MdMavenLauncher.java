package com.hleofxquotes.md;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import com.moneydance.apps.md.controller.Main;

public class MdMavenLauncher extends com.moneydance.apps.md.controller.Main {
    public static void main(String[] args) {

        // Make it portable by setting home directory to current directory
        File cwd = new File(".");
        File homeDir = new File(cwd, "moneyDance");
        homeDir.mkdirs();

        String userHomeDirectory = homeDir.getAbsolutePath();
        System.setProperty("user.home", userHomeDirectory);
        System.out.println("user.home=" + System.getProperty("user.home"));

        Main.DEBUG = true;
        ArrayList<String> argList = new ArrayList<String>(Arrays.asList(args));
        argList.add("-d");
        args = argList.toArray(args);

        com.moneydance.apps.md.controller.Main.main(args);
    }

}
